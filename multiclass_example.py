import ml
import random

random.seed(1)

# examples are of the form (clas,features)
examples = [
        (0,["ab","def","qw"]),
        (1,["qq","qw","qe"]),
        (0,["koo","ab","lop"]),
        (2,["ba","ab","do"]),
        (1,["go","qq","og"])]

# create multiclass parameters vector
W = ml.MultitronParameters(3) # 3 classes

for iteration in xrange(5):
    random.shuffle(examples) # usually a good idea to shuffle examples at every round.
    for clas,features in examples:
        W.tick() # must do this every prediction to keep track of averaging.
        scores = W.get_scores(features)
        # scores are a dictionary from class to score
        predicted_class = max([(s,i) for i,s in scores.iteritems()])[1]
        if predicted_class != clas:
            W.add(features, clas, 1)
            W.add(features, predicted_class, -1)

# now we have a trained model. Let's save it.
W.dump(file("model","w"))
W.dump_fin(file("averaged_model","w")) # the averaged version is usually better.
# have a look at the 'model' and 'averaged_model' files and see if you understand them.

# now let's use the model to predict on some test examples:
W2 = ml.MulticlassModel("averaged_model")

best_class, scores = W2.predict(["ab","qw","koo"])
print "prediction is:", best_class
print "scores are:", scores
print

good = bad = 0.0
print "features\t\tgold\tprediction"
for (gold, features) in examples:
    best_class, scores = W2.predict(features)
    print features,"\t",gold,"\t",best_class
    if best_class == gold: good += 1
    else: bad += 1

print "classified %d out of %d the training examples correctly" % (good, good + bad)
